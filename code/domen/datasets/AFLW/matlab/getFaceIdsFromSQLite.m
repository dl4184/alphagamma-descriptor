function faceids = getFaceIdsFromSQLite( dbfile )
%GETFACEIDSFROMSQLITE 
%   function returns all ids of annotated images in sqlite database
    mksqlite('open',dbfile);
    fidQuery = ['SELECT face_id FROM Faces'];
    faceids = mksqlite(fidQuery);
    faceids=int32(cell2mat(struct2cell(faceids)));
end

