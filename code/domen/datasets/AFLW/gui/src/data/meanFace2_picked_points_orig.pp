<!DOCTYPE PickedPoints>
<PickedPoints>
 <DocumentData>
  <DateTime time="09:40:34" date="2010-03-08"/>
  <User name="Administrator"/>
  <DataFileName name="C:/software/PoseEstimationAnnotationGui/data/meanFace2.wrl"/>
  <templateName name="new Template"/>
 </DocumentData>
 <point x="-0.072" y="-0.346" z="-0.333" active="0" name="mouthPointIn3DModelSpace"/>
 <point x="-0.296" y="-0.348" z="-0.472" active="0" name="mouthLeftCornerPointIn3DModelSpace"/>
 <point x="0.156" y="-0.366" z="-0.417" active="0" name="mouthRightCornerPointIn3DModelSpace"/>
 <point x="-0.361" y="0.309" z="-0.521" active="0" name="leftEyePointIn3DModelSpace"/>
 <point x="-0.4796" y="0.3" z="-0.604" active="0" name="leftEyeLeftCornerPointIn3DModelSpace"/>
 <point x="-0.221" y="0.296" z="-0.512" active="0" name="leftEyeRightCornerPointIn3DModelSpace"/>
 <point x="0.235" y="0.296" z="-0.46" active="0" name="rightEyePointIn3DModelSpace"/>
 <point x="0.105" y="0.291" z="-0.474" active="0" name="rightEyeLeftCornerPointIn3DModelSpace"/>
 <point x="0.369" y="0.285" z="-0.512" active="0" name="rightEyeRightCornerPointIn3DModelSpace"/>
 <point x="-0.17" y="-0.08" z="-0.27" active="0" name="leftNosePointIn3DModelSpace"/>
 <point x="-0.071" y="-0.023" z="-0.13" active="0" name="nosePointIn3DModelSpace"/>
 <point x="0.17" y="-0.08" z="-0.27" active="0" name="rightNosePointIn3DModelSpace"/>
 <point x="-0.649" y="-0.198" z="-1.269" active="0" name="leftEarPointIn3DModelSpace"/>
 <point x="0.593" y="-0.236" z="-1.119" active="0" name="rightEarPointIn3DModelSpace"/>
 <point x="-0.44" y="0.502" z="-0.469" active="0" name="leftBrowCenterPointIn3DModelSpace"/>
 <point x="0.283" y="0.486" z="-0.376" active="0" name="rightBrowCenterPointIn3DModelSpace"/>
 <point x="-0.575" y="0.479" z="-0.64" active="0" name="leftBrowLeftCornerPointIn3DModelSpace"/>
 <point x="-0.207" y="0.444" z="-0.388" active="0" name="leftBrowRightCornerPointIn3DModelSpace"/>
 <point x="0.11" y="0.445" z="-0.358" active="0" name="rightBrowLeftCornerPointIn3DModelSpace"/>
 <point x="0.46" y="0.472" z="-0.51" active="0" name="rightBrowRightCornerPointIn3DModelSpace"/>
 <point x="-0.068" y="-0.703" z="-0.387" active="0" name="chinPointIn3DModelSpace"/>
</PickedPoints>
