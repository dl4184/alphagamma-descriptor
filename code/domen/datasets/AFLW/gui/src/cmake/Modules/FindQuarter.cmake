# - Try to find the Quarter library
# Once done this will define
#
#  QUARTER_FOUND - system has QUARTER
#  QUARTER_INCLUDE_DIR - the QUARTER include directory
#  QUARTER_LIBRARIES - Link these to use QUARTER
#  QUARTER_LINK_DIRECTORIES - link directories, useful for rpath
#
# Copyright (c) 2006, Alexander Neundorf, <neundorf@kde.org>
# Copyright (c) 2007, Werner Trobin, <trobin@kde.org>
# Copyright (c) 2007, Markus Grabner, <grabner@icg.tugraz.at>
# Copyright (c) 2008, Alexander Bornik, <bornik@icg.tugraz.at>
#
# Redistribution and use is allowed according to the terms of the BSD license.
# For details see the accompanying COPYING-CMAKE-SCRIPTS file.

if (QUARTER_INCLUDE_DIR AND QUARTER_LIBRARIES)

  # in cache already
  SET(QUARTER_FOUND TRUE)

else (QUARTER_INCLUDE_DIR AND QUARTER_LIBRARIES)

  FIND_PATH(QUARTER_INCLUDE_DIR Quarter/Quarter.h
   PATHS
   /usr/include
   /usr/local/include
   $ENV{COINDIR}/include
   $ENV{QUARTER_ROOT}/include
   DOC "The include directory of the Quarter library"
   )

  if (WIN32)
    if (QUARTER_INCLUDE_DIR)
      SET(QUARTER_LIBRARIES quarter1 CACHE STRING "Quarter library name")
      SET(QUARTER_DEFINITIONS "-DQUARTER_DLL" CACHE STRING "Quarter definitions")
    endif(QUARTER_INCLUDE_DIR)
  else (WIN32)
    FIND_LIBRARY(QUARTER_LIBRARIES Quarter
     /usr/lib
     /usr/lib64
     /usr/local/lib
     /usr/local/lib64
     $ENV{QUARTER_ROOT}/lib
     $ENV{QUARTER_ROOT}/lib64
     $ENV{COINDIR}/lib
     $ENV{COINDIR}/lib64
    )
    SET(QUARTER_DEFINITIONS "" CACHE STRING "Quarter definitions")
  endif(WIN32)

  if (WIN32)
    if (QUARTER_INCLUDE_DIR)
        SET(QUARTER_LINK_DIRECTORIES $ENV{COINDIR}/lib CACHE STRING "Quarter library directory")
    endif(QUARTER_INCLUDE_DIR)
  else(WIN32)
    FIND_PATH(QUARTER_LIBRARY_DIR libQuarter.so
      /usr/lib
      /usr/lib64
      /usr/local/lib
      /usr/local/lib64
      $ENV{QUARTER_ROOT}/lib
      $ENV{QUARTER_ROOT}/lib64
      $ENV{COINDIR}/lib
      $ENV{COINDIR}/lib64
    ) 
  endif(WIN32)


  if (QUARTER_INCLUDE_DIR)
      SET(QUARTER_FOUND TRUE)
    else (QUARTER_INCLUDE_DIR)
      SET (QUARTER_FOUND FALSE)
    endif (QUARTER_INCLUDE_DIR)

      
  if (QUARTER_FOUND)
    if (NOT QUARTER_FIND_QUIETLY)
      message(STATUS "Found Quarter: ${QUARTER_LIBRARIES}")
    endif (NOT QUARTER_FIND_QUIETLY)
  else (QUARTER_FOUND)
    if (QUARTER_FIND_REQUIRED)
      message(FATAL_ERROR "Could NOT find Quarter")
    endif (QUARTER_FIND_REQUIRED)
  endif (QUARTER_FOUND)

  MARK_AS_ADVANCED(QUARTER_DEFINITIONS)
  
endif (QUARTER_INCLUDE_DIR AND QUARTER_LIBRARIES)

get_filename_component(QUARTER_USE_FILE "${CMAKE_CURRENT_LIST_FILE}" PATH)
set(QUARTER_USE_FILE ${QUARTER_USE_FILE}/UseQuarter.cmake)
