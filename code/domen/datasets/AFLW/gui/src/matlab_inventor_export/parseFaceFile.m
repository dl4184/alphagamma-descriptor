function [faces] = parseFaceFile(path)

fid = fopen(path);

faces = [];
while(~feof(fid))
    tline = fgetl(fid);
    
    A = strread(tline,'%d','delimiter',',');
    
    A = A';
    if (length(A) < 3 || length(A) > 4)
        disp( ['problem in mesh faces at line: ' tline] );
    end
    if(length(A) == 3) %only triangle found
        A = [A nan];
    end
    faces = [faces; ... %quadrangle found
             A];
end

fclose(fid);
end