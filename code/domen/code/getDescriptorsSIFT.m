clear ;
close all;

sift = vicos.descriptor.SIFT();
keypoint_detector = vicos.keypoint_detector.SIFT();
cells=[1,2,4,8];
cellSize=[16/1, 32/2, 48/4, 64/8];
[I, landmarks]=faceExtraction('AFLW/data/flickr/0/image00013.jpg','kazemi');
fieldNames={'desc1x1','desc2x2','desc4x4','desc8x8'};
descriptors=struct('desc1x1',{0},'desc2x2',{0},'desc4x4',{0},'desc8x8',{0});

landmark=landmarks(:,1);


P_min=int16(landmark-cellSize(1)/2+0.5);
P_max=int16(landmark+cellSize(1)/2-0.5);

I1= I(P_min(2):P_max(2),P_min(1):P_max(1),:);
%I1=rgb2gray(I1);

kpts = keypoint_detector.detect(I1);
desc = sift.compute(I1, kpts);
descriptors=setfield(descriptors,'desc1x1',desc);
for i=2:4
    j_index=0;
    z_index=0;
    c1={};
    for j=-cells(i)/2:cells(i)/2-1
        for z=-cells(i)/2:cells(i)/2-1
            P_min=int16([(landmark(1)+ cellSize(i)*j);(landmark(2)+ cellSize(i)*z)]);
            P_max=P_min+cellSize(i)-1;
            
            
            I1=I(P_min(2):P_max(2),P_min(1):P_max(1),:);
            kpts = keypoint_detector.detect(I1);
            desc = sift.compute(I1, kpts);
            size(desc)
            c1{(j_index*cells(i)+z_index+1)}=desc;
            z_index=z_index+1;
        end
        j_index=j_index+1;
    end
    descriptors=setfield(descriptors,fieldNames{i},c1)
end