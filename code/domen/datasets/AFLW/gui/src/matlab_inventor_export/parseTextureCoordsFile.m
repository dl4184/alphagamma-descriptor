function [textureCoords] = parseTextureCoordsFile(path)

fid = fopen(path);

textureCoords = [];
while(~feof(fid))
    tline = fgetl(fid);
    
    A = strread(tline,'%f','delimiter',' ');
    
    A = A';
    if(length(A) == 6) %only triangle found
        A = [A nan nan];
    end
    if (isempty(A))
        continue;
    end
    textureCoords = [textureCoords; ... %quadrangle found
             A];
end

fclose(fid);
end