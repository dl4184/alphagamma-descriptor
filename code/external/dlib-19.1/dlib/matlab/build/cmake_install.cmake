# Install script for directory: /home/domen/Desktop/alphagamma-descriptor/code/external/dlib-19.1/dlib/matlab

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/domen/Desktop/alphagamma-descriptor/code/external/dlib-19.1/dlib/matlab/mex")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/home/domen/Desktop/alphagamma-descriptor/code/external/dlib-19.1/dlib/matlab/mex/face_detection.mexa64" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/domen/Desktop/alphagamma-descriptor/code/external/dlib-19.1/dlib/matlab/mex/face_detection.mexa64")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/home/domen/Desktop/alphagamma-descriptor/code/external/dlib-19.1/dlib/matlab/mex/face_detection.mexa64"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/domen/Desktop/alphagamma-descriptor/code/external/dlib-19.1/dlib/matlab/mex/face_detection.mexa64")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/domen/Desktop/alphagamma-descriptor/code/external/dlib-19.1/dlib/matlab/mex" TYPE MODULE FILES "/home/domen/Desktop/alphagamma-descriptor/code/external/dlib-19.1/dlib/matlab/build/face_detection.mexa64")
  if(EXISTS "$ENV{DESTDIR}/home/domen/Desktop/alphagamma-descriptor/code/external/dlib-19.1/dlib/matlab/mex/face_detection.mexa64" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/home/domen/Desktop/alphagamma-descriptor/code/external/dlib-19.1/dlib/matlab/mex/face_detection.mexa64")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/home/domen/Desktop/alphagamma-descriptor/code/external/dlib-19.1/dlib/matlab/mex/face_detection.mexa64"
         OLD_RPATH "/opt/matlab/R2016a/bin/glnxa64:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/home/domen/Desktop/alphagamma-descriptor/code/external/dlib-19.1/dlib/matlab/mex/face_detection.mexa64")
    endif()
  endif()
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/domen/Desktop/alphagamma-descriptor/code/external/dlib-19.1/dlib/matlab/build/dlib_build/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/home/domen/Desktop/alphagamma-descriptor/code/external/dlib-19.1/dlib/matlab/build/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
