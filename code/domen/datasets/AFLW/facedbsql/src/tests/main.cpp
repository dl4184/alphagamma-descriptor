/*
 * main.cpp
 *
 *  Created on: 23.07.2010
 *      Author: pwohlhart
 */

#include <iostream>
#include <sqlite3.h>

#include "../dbconn/SQLiteDBConnection.h"
#include "../facedata/FaceData.h"
#include "../querys/FaceDBQuery.h"
#include "../querys/FaceIDByPoseQuery.h"
#include "../querys/FaceDataByIDsQuery.h"
#include "../querys/FaceIDByAnnotatedFeatureQuery.h"

using namespace std;

static int callback(void *NotUsed, int argc, char **argv, char **azColName)
{
	int i;
	cout << "----------------- begin record" << endl;
	for (i = 0; i < argc; i++)
	{
		cout << azColName[i] << " = " << (argv[i] ? argv[i] : "NULL") << endl;
	}
	cout << "----------------- end record " << endl;
	return 0;
}



int
main (int argc, char **argv)
{
	cout << "testing sqlite connection" << endl;

	//string dbFilename = "~/.gvfs/public auf jupiter/projects/mdl/data/facedb-data";
	//string dbFilename = "../db/facedb-data.db";
	string dbFilename = "/home/pwohlhart/work/data/facedb/facedb-data.db";

	SQLiteDBConnection *sqlConn = new SQLiteDBConnection();

	if (sqlConn->open(dbFilename))
	{
		FeatureCoordTypes fcTypes;
		fcTypes.load(sqlConn);
		fcTypes.debugPrint();

		cout << "-------------------------------" << endl;

		FaceIDByPoseQuery fidq;
		fidq.setAngles(0.0, 1.0, 0.0, 1.0, 0.0, 1.0);
		fidq.exec(sqlConn);
		fidq.debugPrint();

		cout << "-------------------------------" << endl;

		FaceDataByIDsQuery fdq;
		fdq.queryIds = fidq.face_ids;
		fdq.exec(sqlConn);

		FaceData *fd = fdq.data[fdq.queryIds[0]];

		//FeaturesCoords *fc = new FeaturesCoords(fd);
		//fc->load(sqlConn);
		fd->loadFeatureCoords(sqlConn);
		FeaturesCoords *fc = fd->getFeaturesCoords();
		//fc->debugPrint();

		fd->debugPrint();

		//FeaturesCoords *fc2 = new FeaturesCoords(*fc);
		//fc2->debugPrint();

		cout << "-------------------------------" << endl;
		cout << "Get all faces that have features 1 and 2 annotated: ";

		FaceIDByAnnotatedFeatureQuery fidByFeat;
		fidByFeat.queryFeatureIDs.push_back(1);
		fidByFeat.queryFeatureIDs.push_back(2);
		fidByFeat.exec(sqlConn);
		for (unsigned int i = 0; i < fidByFeat.resultFaceIds.size(); ++i)
			cout << fidByFeat.resultFaceIds[i] << ", ";
		cout << endl;

		cout << "Get all faces that have features 1,2 and 3 annotated: ";
		fidByFeat.queryFeatureIDs.push_back(3);
		fidByFeat.exec(sqlConn);
		for (unsigned int i = 0; i < fidByFeat.resultFaceIds.size(); ++i)
			cout << fidByFeat.resultFaceIds[i] << ", ";
		cout << endl;

		sqlConn->close();
	}

	cout << "done" << endl;

	return 0;
}
