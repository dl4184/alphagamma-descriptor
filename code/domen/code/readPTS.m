function [ points ] = readPTS( points_file_path )
%Function that reads .pts file and returns 20x2 array 
%0 = right eye pupil
%1 = left eye pupil
%2 = right mouth corner
%3 = left mouth corner
%4 = outer end of right eye brow
%5 = inner end of right eye brow
%6 = inner end of left eye brow
%7 = outer end of left eye brow
%8 = right temple
%9 = outer corner of right eye
%10 = inner corner of right eye
%11 = inner corner of left eye
%12 = outer corner of left eye
%13 = left temple
%14 = tip of nose
%15 = right nostril
%16 = left nostril
%17 = centre point on outer edge of upper lip
%18 = centre point on outer edge of lower lip
%19 = tip of chin

    [fileID,errmsg] =fopen(points_file_path);
    if  isempty(errmsg)
        npoints=textscan(fileID,'%s %f',1,'HeaderLines',1);
        rows=textscan(fileID,'%f %f',npoints{2},'MultipleDelimsAsOne',2,'Headerlines',2);
        points=cell2mat(rows);
        fclose(fileID);
    else
        points=[];
    end
    
end

