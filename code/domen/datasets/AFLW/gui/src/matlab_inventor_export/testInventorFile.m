function [] = testInventorFile(filename)

%parse the makehuman files
[pts] = parseVerticeFile('c:\Program Files\MakeHuman 0.9.1 RC1\data\base.vertices');
[faces] = parseFaceFile('c:\Program Files\MakeHuman 0.9.1 RC1\data\base.faces'); 
[textureCoords] = parseTextureCoordsFile('c:\Program Files\MakeHuman 0.9.1 RC1\data\base.uv'); 

faces = faces';
textureCoords = textureCoords';

size_faces = size(faces)
size_texture_coords = size(textureCoords)

max_face_index = max(faces(:))
min_face_index = min(faces(:))

%calculate the texture coords

textureCoordsPerVertex = zeros(max_face_index+1,2);

for k = 0 : max_face_index-1
%for k = 6117 : 6119
    indices = find(faces == k);
    first_index = indices(1);
    tempTexCoord = [textureCoords(2*first_index-1) 1.0-textureCoords(2*first_index)];
    textureCoordsPerVertex(k+1,:) = tempTexCoord;
end

%textureCoordsPerVertex(6117,:)
%textureCoordsPerVertex(6118,:)
%textureCoordsPerVertex(6119,:)

size_points = size(pts)
size_textureCoordsPerVertex = size(textureCoordsPerVertex)


% calculate the surface normals

normalsPerVertex = zeros(size(pts,2),3);

for ptsIndex = 1 : size(pts,2)
    indices = find(faces == (ptsIndex-1));
    size_indices = size(indices);
    if (size_indices(1) >= 3)
        indicesCols = ceil( indices ./ 4 );
        avgNormal = [0 0 0];
        for i = 1 : 3
            if (indicesCols(i) < 1 || indicesCols(i) > size_faces(2))
                ptsIndex-1
                indices
                indicesCols
                faces(1,indicesCols(i))+1
                faces(2,indicesCols(i))+1
                faces(3,indicesCols(i))+1
                p1 = pts(:,faces(1,indicesCols(i))+1)
                p2 = pts(:,faces(2,indicesCols(i))+1)
                p3 = pts(:,faces(3,indicesCols(i))+1)
            end
            current_pt1 = pts(:,faces(1,indicesCols(i))+1);
            current_pt2 = pts(:,faces(2,indicesCols(i))+1);
            current_pt3 = pts(:,faces(3,indicesCols(i))+1);
            tempNormal = cross( (current_pt2-current_pt1)', (current_pt3-current_pt1)');
            % normalize normal vector
            tempNormal = tempNormal ./ norm(tempNormal);
            
            % do a plane fitting from the points
            x = [current_pt1(1) current_pt2(1) current_pt3(1)]';
            y = [current_pt1(2) current_pt2(2) current_pt3(2)]';
            z = [current_pt1(3) current_pt2(3) current_pt3(3)]';
            A=[x,y,z,ones(length(x),1)];
            [U,S,V]=svd(A);
            ss=diag(S);
            i=find(ss==min(ss)); % find position of minimal singular value
            coeff=V(:,min(i)); % this may be multiple
            coeff=coeff/norm(coeff(1:3),2);

            if (coeff(4) > 0)
                tempNormal = -1 * tempNormal;
            end
            
            avgNormal = avgNormal + tempNormal;
        end
        avgNormal ./ 3;
        normalsPerVertex(ptsIndex,:) = avgNormal ./ norm(avgNormal);
    else
        disp( ['ptsIndex '  num2str(ptsIndex) ' has less than 3 occurences in the face datastructure!' ] );
    end
end



%for k = 0 : 5
%    %max_face_index-1
%    current_face_index = k;
%    current_face_index_pt1 = faces(1,k+1)
%    current_face_index_pt2 = faces(2,k+1)
%    current_face_index_pt3 = faces(3,k+1)
%    current_pt1 = pts(:,current_face_index_pt1+1)
%    current_pt2 = pts(:,current_face_index_pt2+1)
%    current_pt3 = pts(:,current_face_index_pt3+1)
%    tempNormal = cross( (current_pt2-current_pt1)', (current_pt3-current_pt1)');
%    % normalize normal vector
%    tempNormal = tempNormal ./ norm(tempNormal)
%    %indices = find(faces == k)
%    %first_index = indices(1:3)
%    %tempNormal = [0 0 1];
%    %normalsPerVertex(k+1,:) = tempNormal;
%end



%colors = repmat([1 1 1], 1, size(pts,2)); %assign each point the same color

fid = fopen(filename, 'w');

%create header
header = '#Inventor V2.0 ascii';
fprintf(fid, '%s\n\n', header);
fprintf(fid, '%s\n\n','Separator {');

%set background material
fprintf(fid,'%s\n' ,'Material {');
fprintf(fid,'%s\n' ,'diffuseColor [ 1 1 1 ]');
fprintf(fid,'%s\n' ,'}');

%plot the vertices
if(~isempty(pts))
    
    % texture image
    fprintf(fid, '%s\n','    Texture2 {');
    fprintf(fid, '%s\n','       filename body_color.tif');
    fprintf(fid, '%s\n','    }');
    
    %colors
    %fprintf(fid, '%s\n','    Material {');
    %fprintf(fid, '%s\n','    diffuseColor [');
    %fprintf(fid, '%f %f %f,\n',colors);
    %fprintf(fid, '%s\n','    ]');
    %fprintf(fid, '%s\n','    }');
    %fprintf(fid, '%s\n','    MaterialBinding { value PER_PART }');


    %points
    fprintf(fid, '%s\n','    Coordinate3 {');
    fprintf(fid, '%s\n','        point [');

    fprintf(fid, '%f %f %f,\n',pts);
    fprintf(fid, '%s\n','        ]');
    fprintf(fid, '%s\n','    }');

    fprintf(fid, '%s\n','    PointSet { }');    
end

%if (~isempty(normalsPerVertex))
%    %normals
%    fprintf(fid, '%s\n','    Normal {');
%    fprintf(fid, '%s\n','        vector [');

%    fprintf(fid, '%f %f %f,\n',normalsPerVertex);
%    fprintf(fid, '%s\n','        ]');
%    fprintf(fid, '%s\n','    }');
%end

if( ~isempty(faces) && ~isempty(textureCoords) )
    %texture coords
    fprintf(fid, '%s\n','    TextureCoordinate2 {');
    fprintf(fid, '%s\n','        point [');

    fprintf(fid, '%f %f,\n',textureCoordsPerVertex');
    
    fprintf(fid, '%s\n','        ]');
    fprintf(fid, '%s\n','    }');
end

if( ~isempty(faces))
    %faces
    fprintf(fid, '%s\n','    IndexedFaceSet {');
    fprintf(fid, '%s\n','        coordIndex [');

    for k = 1 : size(faces,2)
        mask = isnan(faces(:,k)); %some of them might be nan (triangle, quadrangle mix)
        if(sum(mask) == 0)
            fprintf(fid, '%d %d %d %d %d,\n',[faces(:,k); -1]);
        else
            fprintf(fid, '%d %d %d %d,\n',[faces(~mask,k); -1]);
        end
    end
 
    fprintf(fid, '%s\n','        ]');
    
    % repeat coord index for texture coord index
    fprintf(fid, '%s\n','        textureCoordIndex [');

    for k = 1 : size(faces,2)
        mask = isnan(faces(:,k)); %some of them might be nan (triangle, quadrangle mix)
        if(sum(mask) == 0)
            fprintf(fid, '%d %d %d %d %d,\n',[faces(:,k); -1]);
        else
            fprintf(fid, '%d %d %d %d,\n',[faces(~mask,k); -1]);
        end
    end
    
    fprintf(fid, '%s\n','        ]');
    
    % repeat coord index for normal index
    fprintf(fid, '%s\n','        normalIndex [');

    for k = 1 : size(faces,2)
        mask = isnan(faces(:,k)); %some of them might be nan (triangle, quadrangle mix)
        if(sum(mask) == 0)
            fprintf(fid, '%d %d %d %d %d,\n',[faces(:,k); -1]);
        else
            fprintf(fid, '%d %d %d %d,\n',[faces(~mask,k); -1]);
        end
    end
 
    fprintf(fid, '%s\n','        ]');
    
    fprintf(fid, '%s\n','    }');
    
    fprintf(fid, '%s\n','    }');
end

fid = fclose(fid);
end

