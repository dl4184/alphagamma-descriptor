function [ face, landmarkM ] =  faceExtraction( imgLocation, detectorType )
%function that extracts face from image

DOMEN_FOLDER=getPath();


face=[];
switch detectorType
    case 'uricar'
       modelClandmark='jointmv/PART_fixed_JOINTMV_FRONTAL.xml'; 
       landmarks  = landmarkDetector( imgLocation, modelClandmark, detectorType );
       %TODO 
       
    case 'kazemi' 
        modelKazemi='shape_predictor_68_face_landmarks.dat';
        landmarks  = landmarkDetector( imgLocation, modelKazemi, detectorType );
        if isempty(landmarks)==0
            eyeR= mean(landmarks(:,37:42),2);
            eyeL= mean (landmarks(:,43:48),2);
            
            %we rotate image so both eyes are in the same line
            angle=getAngle(eyeR,eyeL);
            I=imread(strcat(DOMEN_FOLDER,'/datasets/',imgLocation));
            rotMatrix = affine2d([cosd(angle) -sind(angle) 0;sind(angle) cosd(angle) 0; 0 0 1]);
            [RI, ref] = imwarp(I,rotMatrix);
            
            
            %transforming point to right location after image rotation
            landmarks=getPointsAfterRotation(landmarks,rotMatrix,ref);
            eyeR= mean(landmarks(:,37:42),2);
            eyeL= mean (landmarks(:,43:48),2);
            
            pointBetweenEyes=mean([eyeR eyeL],2);
            noseC=landmarks(:,31);
            
            faceCenter=mean([pointBetweenEyes noseC],2);%chin*0.4+pointBetweenEyes*0.6;%
                        
            
            %dBetweenEyes=leftEye(1)-rightEye(1);
            faceWidth=max(landmarks(1,:))-min(landmarks(1,:));
            facePatchSize=faceWidth*224/128;
            
            P_min=int16(faceCenter-facePatchSize/2+0.5);
            
            P_max=int16(faceCenter+facePatchSize/2-0.5);
            
            %we place landmark coorindates into face coordinates
            landmarks(1,:)=landmarks(1,:)-double(P_min(1));
            landmarks(2,:)=landmarks(2,:)-double(P_min(2));
            
            
            face=RI(P_min(2):P_max(2), P_min(1):P_max(1),:);
            face=imresize(face,[224 224]);
            
            %we calculate landmarks coordinates after image resize
            landmarks=landmarks*224/facePatchSize;
            
            eyeL= mean (landmarks(:,43:48),2);
            eyeR= mean(landmarks(:,37:42),2);
            noseC=landmarks(:,31);
            mouthL=landmarks(:,55);
            mouthC=(landmarks(:,63)+landmarks(:,67))/2;
            mouthR=landmarks(:,49);
            chinC=landmarks(:,9);
            
            landmarkM=[eyeL, eyeR, noseC, mouthL, mouthC, mouthR, chinC];
        end

end

