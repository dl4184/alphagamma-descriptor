#####################################################################
#
#  FindFaceDBSql.cmake
#


IF (WIN32)
	SET (FaceDBSql_DIR $ENV{FACEDBSQL_DIR} CACHE PATH "Base Directory of FaceDBSql installation")

	SET(FaceDBSql_INCLUDE_DIRS  ${FaceDBSql_DIR}/include/facedbsql/)
	SET(FaceDBSql_LINK_DIRECTORIES  ${FaceDBSql_DIR}/lib/)
	SET(FaceDBSql_LIBRARIES debug facedbsqllibd optimized facedbsqllib )

ELSE()

	FIND_PATH(FaceDBSql_INCLUDE_DIR dbconn/SQLiteDBConnection.h
          HINTS $ENV{FACEDBSQL_DIR}/include/facedbsql
          PATH_SUFFIXES facedbsql 
	)

	FIND_LIBRARY( FaceDBSql_LIBRARY NAMES facedbsqllib facedbsqllibd 
	 HINTS $ENV{FACEDBSQL_DIR}/lib )

	set(FaceDBSql_LIBRARIES ${FaceDBSql_LIBRARY} )
	set(FaceDBSql_INCLUDE_DIRS ${FaceDBSql_INCLUDE_DIR} )

	include(FindPackageHandleStandardArgs)
	# handle the QUIETLY and REQUIRED arguments and set FaceDBSql_FOUND to TRUE
	# if all listed variables are TRUE
	find_package_handle_standard_args(FaceDBSql  DEFAULT_MSG
                                  FaceDBSql_LIBRARY FaceDBSql_INCLUDE_DIR)

ENDIF ()

mark_as_advanced( FaceDBSql_INCLUDE_DIR FaceDBSql_LIBRARY )


