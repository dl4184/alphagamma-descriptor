

#include <iostream>
#include <string>

#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_io.h>

#include "call_matlab.h"
#include "dlib/matrix.h"

using namespace dlib;
using namespace std;

void mex_function (const string shape_predictor_68_face_landmarks_location, const string img_location,matlab_struct& output
){
// We need a face detector.  We will use this to get bounding boxes for
// each face in an image.
frontal_face_detector detector = get_frontal_face_detector();
// And we also need a shape_predictor.  This is the tool that will predict face
// landmark positions given an image and face bounding box.  Here we are just
// loading the model from the shape_predictor_68_face_landmarks.dat file you gave
// as a command line argument.
shape_predictor sp;
deserialize(shape_predictor_68_face_landmarks_location) >> sp;

	
array2d<rgb_pixel> img;
load_image(img, img_location);
int old_nc = img.nc();
int old_nr = img.nr();
// Make the image larger so we can detect small faces.
pyramid_up(img);
int new_nc = img.nc();
int new_nr = img.nr();
// Now tell the face detector to give us a list of bounding boxes
// around all the faces in the image.
std::vector<rectangle> dets = detector(img);

int no_landmarks;
double landmark_loc;
point p;
std::vector<full_object_detection> shapes;

for (unsigned long j = 0; j < dets.size(); ++j){
	full_object_detection shape = sp(img, dets[j]);
		no_landmarks=shape.num_parts() & INT_MAX;

		matrix<double> m(2,no_landmarks);
		for(int i=0;i<no_landmarks;i++){
			p=shape.part(i);
			m(0,i)=p(0)*(double)old_nc/new_nc;
			m(1,i)=p(1)*(double)old_nr/new_nr;
		}

	// Here we just store them in shapes so we can
	// put them on the screen.
	shapes.push_back(shape);	

		output["face_"+to_string(j+1)] = m ;
	}


	output["noDetectedFaces"] = dets.size();
}
#include "mex_wrapper.cpp"
