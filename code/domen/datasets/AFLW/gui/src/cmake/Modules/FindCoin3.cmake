# - Try to find the Coin library
# Once done this will define
#
#  COIN_FOUND - system has COIN
#  COIN_INCLUDE_DIR - the COIN include directory
#  COIN_LIBRARIES - Link these to use COIN
#  COIN_LINK_DIRECTORIES - link directories, useful for rpath
#  COIN_DEFINITIONS - Compiler switches required for using COIN
#
# Copyright (c) 2006, Alexander Neundorf, <neundorf@kde.org>
# Copyright (c) 2007, Werner Trobin, <trobin@kde.org>
#
# Redistribution and use is allowed according to the terms of the BSD license.
# For details see the accompanying COPYING-CMAKE-SCRIPTS file.

if(COIN_INCLUDE_DIR AND COIN_LIBRARIES)

  # in cache already
  SET(COIN_FOUND TRUE)

else(COIN_INCLUDE_DIR AND COIN_LIBRARIES)

  if(WIN32)
    
    # check whether the COINDIR environment variable is set and points to a 
    # valid windows coin installation
    FIND_PATH( COIN_INCLUDE_DIR Inventor/SoDB.h PATHS $ENV{COINDIR}/include
      DOC "The include directory of the Coin library" )
    
    if(COIN_INCLUDE_DIR)
      SET(COIN_LINK_DIRECTORIES $ENV{COINDIR}/lib CACHE PATH "Coin link directory")
      SET(COIN_LIBRARIES coin3 CACHE STRING "Coin library name")
      SET(COIN_DEFINITIONS -DCOIN_DLL CACHE STRING "Coin definitions")
      SET(COIN_FOUND TRUE)
    else(COIN_INCLUDE_DIR)
      SET (COIN_FOUND FALSE)
    endif(COIN_INCLUDE_DIR)

  else(WIN32)

    # coin-config should be in your path anyhow, usually no need to set COINDIR
    FIND_PROGRAM(COINCONFIG_EXECUTABLE coin-config
                 $ENV{COINDIR}
                 $ENV{COINDIR}/bin )

    # check whether coin-config has been found:
    if (COINCONFIG_EXECUTABLE)
      EXEC_PROGRAM(${COINCONFIG_EXECUTABLE} ARGS "--libs"
                   OUTPUT_VARIABLE COIN_LIBRARIES_OUT )
      SET(COIN_LIBRARIES "${COIN_LIBRARIES_OUT}" CACHE STRING "Coin library name")
      EXEC_PROGRAM(${COINCONFIG_EXECUTABLE} ARGS "--ldflags"
                   OUTPUT_VARIABLE COIN_LINK_DIRECTORIES_OUT )
      SET(COIN_LINK_DIRECTORIES "${COIN_LINK_DIRECTORIES_OUT}" CACHE STRING 
         "Coin link directory")
      EXEC_PROGRAM(${COINCONFIG_EXECUTABLE} ARGS "--includedir"
                   OUTPUT_VARIABLE COIN_INCLUDE_DIR_OUT )
      SET(COIN_INCLUDE_DIR "${COIN_INCLUDE_DIR_OUT}" CACHE STRING
         "The include directory of the Coin library")
      EXEC_PROGRAM(${COINCONFIG_EXECUTABLE} ARGS "--cppflags"
                OUTPUT_VARIABLE COIN_DEFINITIONS_OUT )
		SET(COIN_DEFINITIONS "${COIN_DEFINITIONS_OUT}" CACHE STRING "Coin definitions")
		
      if (COIN_INCLUDE_DIR AND COIN_LIBRARIES)
         set(COIN_FOUND TRUE)
      endif (COIN_INCLUDE_DIR AND COIN_LIBRARIES)
    endif (COINCONFIG_EXECUTABLE)
    
    MARK_AS_ADVANCED(COINCONFIG_EXECUTABLE)
    
  endif(WIN32)
  
  if(COIN_FOUND)
    if (NOT COIN_FIND_QUIETLY)
      message(STATUS "Found Coin: ${COIN_LIBRARIES}")
    endif (NOT COIN_FIND_QUIETLY)
  else(COIN_FOUND)
    if (COIN_FIND_REQUIRED)
      message(FATAL_ERROR "Could NOT find Coin")
    endif (COIN_FIND_REQUIRED)
  endif(COIN_FOUND)

  MARK_AS_ADVANCED(COIN_DEFINITIONS)
  
endif(COIN_INCLUDE_DIR AND COIN_LIBRARIES)

SET(COIN_USE_FILE ${CMAKE_ROOT}/Modules/UseCoin.cmake)
