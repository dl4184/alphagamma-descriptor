clear;
close all;
DOMEN_FOLDER=getPath();

BASE_FILE=strcat(DOMEN_FOLDER,'/datasets/BioID-FaceDB');
FACE_IMAGES_FILE=strcat(BASE_FILE,'/BioID-FaceDatabase-V1.2');
KEYPOINTS_FILE=strcat(BASE_FILE,'/bioid_pts/points_20');

imageNo1='0000';

image_filename1 = strcat(FACE_IMAGES_FILE,'/BioID_',imageNo1,'.pgm');
I1 = imread(image_filename1);

points_file_path1=strcat(KEYPOINTS_FILE,'/bioid_',imageNo1,'.pts');
points1=readPTS(points_file_path1);

%Number of images in database
noImages=1520+1;

D_ALL=zeros(noImages,20,20);
DIAGONAL_ALL=zeros(noImages,20);

FACES_CLASSES={
        [0:9 11:15 18 46:49 146 170:179 308:321 375 376 482 564:566 706:710 918:922 1015:1026 1079:1094 1441:1450 1473:1494]
        [654:659]
        [10 98:114 973:1014]
        [76:89 711:740 861:873 1122:1152]
        [90:97 147:169 556:563 567:585 1319:1358]
        [660:705]
        [16 17]
        [50:75 418:448 1153:1173]
        [115:145 519:555 1451:1472]
        [19:45 893:917]
        [180:221 923:941]
        [222:240 1027:1078]
        [241:259 449:481 1387:1440]
        [260:307 649:651 874:892 1214:1242]
        [652 653 741:778]
        [322:372 374 377:417 586:602 1174:1213]
        [810:860]
        [483:518 779:809 1095:1121]
        [373 942:972]
        [643:648 1359:1386 1515:1520]
        [634:642 1293:1318]
        [603:633 1495:1514]
        [1243:1292]
};
index=7;
prva=cell2mat(FACES_CLASSES(index));

for j=1:length(prva)
    i=prva(j)+1
    imageIndex = sprintf('%04i',i-1);
    D_ALL(i,:,:)=compareImages(I1,points1,imageIndex);
    DIAGONAL_ALL(i,:)=diag(squeeze(D_ALL(i,:,:)));
end

sum_diag=sum(DIAGONAL_ALL,2);

prva_sum=zeros(length(prva),1);

for i=1:length(prva)
    prva_sum(i)=sum_diag(int32(prva(i)+1));
end
prva_sum_sorted=sort(prva_sum);
max(prva_sum)
%[ D_ag ] = compareImages('0000','0001');