%returns the path to domen folder
function path=getPath()
    CURRENT_FOLDER=fileparts(mfilename('fullpath'));
    current_folder_arr=strsplit(CURRENT_FOLDER,'/');
    path=strjoin(current_folder_arr(1:end-1),'/');
end
