clear;
close all;


imgLocation='AFLW/data/flickr/0/image00013.jpg';
detectorType='kazemi';

[I, landmarkM]=faceExtraction(imgLocation, detectorType);

figure(1); clf(1);
imshow(I, [], 'Border', 'tight'); hold on;
plot(landmarkM(1, :), landmarkM(2, :), 'rs', 'LineWidth', 1, 'MarkerSize', 5, 'MarkerFaceColor', 'r');