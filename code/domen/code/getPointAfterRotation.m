function  p  = getPointAfterRotation( p_old, tform ,ref )
    %returns new point location after we used transfomration tform
    [x,y]=transformPointsForward(tform,p_old(1),p_old(2));
    x = x - ref.XWorldLimits(1);
    y = y - ref.YWorldLimits(1);
    p=[x y];
end

