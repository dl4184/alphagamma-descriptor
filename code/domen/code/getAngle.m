function angle = getAngle(p1, p2)
   tmp = (p1(2)-p2(2))/(p1(1)-p2(1));
   angle = atand(tmp);
end
