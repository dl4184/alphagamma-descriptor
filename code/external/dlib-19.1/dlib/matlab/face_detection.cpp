#include <iostream>
#include <string>

#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_io.h>

#include "call_matlab.h"
#include "dlib/matrix.h"

using namespace dlib;
using namespace std;


void mex_function (const string img_location,matlab_struct& output
){
	frontal_face_detector detector = get_frontal_face_detector();
    array2d<unsigned char> img;
    load_image(img, img_location);
	int old_nc = img.nc();
	int old_nr = img.nr();
    pyramid_up(img);
	int new_nc = img.nc();
	int new_nr = img.nr();
	
    std::vector<rectangle> dets = detector(img);
	matrix<double> m(1,4);
	rectangle r;
	output["noDetectedFaces"] = dets.size();
	for (int i = 0; i < dets.size(); ++i){
		r=dets[i];
		
		m=r.left()*(double)old_nr/new_nr,
		  r.top()*(double)old_nc/new_nc,
		  r.right()*(double)old_nr/new_nr,
		  r.bottom()*(double)old_nc/new_nc;
		output["face_"+to_string(i+1)] = m ;
	}
    
	
}
#include "mex_wrapper.cpp"
