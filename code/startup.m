function startup ()
    % Root directory
    root_dir = fileparts(mfilename('fullpath'));

    %% This folder
    addpath(root_dir);
    addpath(fullfile(root_dir, 'paper2017'));

    % OpenCV libraries (only on Windows; on linux, we must put them in
    % LD_LIBRARY_PATH before Matlab is started)
    if ispc()
        opencv_bin_dir = fullfile(fileparts(mfilename('fullpath')), 'external', 'opencv-bin', vicos.utils.opencv.get_arch_id(), vicos.utils.opencv.get_compiler_id(), 'bin');
        setenv('PATH', [ opencv_bin_dir, ';', getenv('PATH')]);
    end
    
    % mexopencv
    addpath( fullfile(root_dir, 'external', 'mexopencv') );

    % lapjv
    addpath( fullfile(root_dir, 'external', 'lapjv') );

    % tight subplot
    addpath( fullfile(root_dir, 'external', 'tight_subplot') );
    
    % clandmark
    clandmark_dir=fullfile(root_dir, 'external', 'clandmark');
    addpath(clandmark_dir);
    addpath( fullfile(clandmark_dir,'matlab_interface') );
    addpath( fullfile(clandmark_dir,'matlab_interface','mex') );
    addpath( fullfile(clandmark_dir,'matlab_interface','functions') );
    
    %dlib
    dlib_dir=fullfile(root_dir, 'external', 'dlib-19.1');
    addpath(dlib_dir);
    addpath(fullfile(dlib_dir,'dlib','matlab'));
    addpath(fullfile(dlib_dir,'dlib','matlab','mex'));
    
    %mksqlite
    mksqlite_dir=fullfile(root_dir, 'external', 'mksqlite-2.5','releases','mksqlite-1.1');
    addpath(mksqlite_dir);
    
    %domen 
    domen_dir=fullfile(root_dir,'domen');
    addpath(domen_dir);
    addpath(fullfile(domen_dir,'code'));
    addpath(fullfile(domen_dir,'datasets'));
    addpath(fullfile(domen_dir,'datasets','AFLW','matlab'))
    addpath(fullfile(domen_dir,'models'));
    
       
    

    %% Turn off warnings
    % Image size warning
    warning('off', 'Images:initSize:adjustingMag');
end
