clear;
close all;
DOMEN_FOLDER=getPath();

%%setting locaiton of model and image
model68=strcat(DOMEN_FOLDER,'/models/shape_predictor_68_face_landmarks.dat');
img_location=strcat(DOMEN_FOLDER,'/datasets/AFLW/data/flickr/0/image00002.jpg');

%%getting landmark locations
output=face_landmark_detector(model68,img_location);
I= imread(img_location);
P= output.face_1;


figure, imshow(I, [], 'Border', 'tight'); hold on;
for i=1:68
    text(P(1, i), P(2, i), cellstr(num2str(i)),'FontSize', 10, 'Color', 'r');
end