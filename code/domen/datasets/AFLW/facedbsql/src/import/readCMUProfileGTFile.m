function [profile_pics_data,frontal_pics_data] = readCMUProfileGTFile(profile_gt_filename,frontal_gt_filename)

    pfid = fopen(profile_gt_filename);
    
    done = false;
    linecount = 0;
    while ~done
        l = fgetl(pfid);
        if ischar(l)
            linecount = linecount + 1;
        else
            done = true;
        end
    end
    
    profile_pics_data = struct('filename', cell(linecount,1), ...
        'reyecorner', cell(linecount,1), ...
        'reye', cell(linecount,1), ...
        'nose', cell(linecount,1), ...
        'nosetip', cell(linecount,1), ...
        'rmouth', cell(linecount,1), ...
        'cmouth', cell(linecount,1), ...
        'chin', cell(linecount,1), ...
        'rightearlobe', cell(linecount,1), ...
        'righteartip', cell(linecount,1), ...
        'leyecorner', cell(linecount,1), ...
        'leye', cell(linecount,1), ...
        'lmouth', cell(linecount,1), ...
        'leftearlobe', cell(linecount,1), ...
        'lefteartip', cell(linecount,1), ...
        'rightearvalid', cell(linecount,1), ...
        'leftearvalid', cell(linecount,1), ...
        'backheadvalid', cell(linecount,1));
    
    fseek(pfid,0,-1);
    for i=1:linecount
        C = textscan(pfid,'%s',1);
        profile_pics_data(i).filename = C{1};
        C = textscan(pfid,'{%s %f %f}');
        for j=1:size(C{1},1)
            profile_pics_data(i).(C{1}{j}) = num2cell([C{2}(j) C{3}(j)]);
        end
        C = textscan(pfid,'<%s %[^>]>');
        for j=1:size(C{1},1)
            profile_pics_data(i).(C{1}{j}) = C{2}(j);
        end
    end

    
    % ------------ frontal
  
    ffid = fopen(frontal_gt_filename);
    
    done = false;
    linecount = 0;
    while ~done
        l = fgetl(ffid);
        if ischar(l)
            linecount = linecount + 1;
        else
            done = true;
        end
    end
    
    frontal_pics_data = struct('filename', cell(linecount,1), ...
         'leye', cell(linecount,1), ...
         'reye', cell(linecount,1), ...
         'nose', cell(linecount,1), ...
         'lmouth', cell(linecount,1), ...
         'cmouth', cell(linecount,1), ...
         'rmouth', cell(linecount,1));
    
    fseek(ffid,0,-1);
    for i=1:linecount
        C = textscan(ffid,'%s',1);
        frontal_pics_data(i).filename = C{1};
        C = textscan(ffid,'{%s %f %f}');
        for j=1:size(C{1},1)
            frontal_pics_data(i).(C{1}{j}) = num2cell([C{2}(j) C{3}(j)]);
        end
    end

    