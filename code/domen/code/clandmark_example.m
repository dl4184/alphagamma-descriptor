clear;
close all;
DOMEN_FOLDER=getPath();

%%reading image
img = strcat(DOMEN_FOLDER,'/datasets/AFLW/data/flickr/0/image00002.jpg');%'/home/domen/Desktop/alphagamma-descriptor/code/domen/code/face.jpg';
I=imread(img);
Ibw = rgb2gray(I);


%% init flandmark
model = strcat(DOMEN_FOLDER,'/models/jointmv/PART_fixed_JOINTMV_FRONTAL.xml');
flandmark = flandmark_class(model);

% Set face bbox
output=face_detection(img);
bbox = output.face_1;
%bbox=[72 72 180 180];
% Detect landmarks

tic
P = flandmark.detect(Ibw, int32(bbox));
t1 = toc;
%fprintf('MEX detect:    Elapsed time %f ms \t %s \n', t1*1000, fname);

% Show output

figure(1); clf(1);
imshow(I, [], 'Border', 'tight'); hold on;
plot(P(1, :), P(2, :), 'rs', 'LineWidth', 1, 'MarkerSize', 5, 'MarkerFaceColor', 'r');

% Destroy flandmark

clear flandmark;  
 