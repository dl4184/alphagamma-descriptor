% Before running, make sure to run startup.m in alphagamma-code folder!

clear;
close all;
DOMEN_FOLDER=getPath();

BASE_FILE=strcat(DOMEN_FOLDER,'/datasets/BioID-FaceDB');

image1_filename = strcat(BASE_FILE,'/BioID-FaceDatabase-V1.2/BioID_0000.pgm');
image2_filename = strcat(BASE_FILE,'/BioID-FaceDatabase-V1.2/BioID_0001.pgm');

I1 = imread(image1_filename);
I2 = imread(image2_filename);

% Create SIFT keypoint detector
keypoint_detector = vicos.keypoint_detector.SIFT();

% Detect keypoints
kpts1 = keypoint_detector.detect(I1);
kpts2 = keypoint_detector.detect(I2);

% Create SIFT descriptor
sift = vicos.descriptor.SIFT();
desc1 = sift.compute(I1, kpts1);
desc2 = sift.compute(I2, kpts2);

% Descriptor distances
D_sift = sift.compute_pairwise_distances(desc1, desc2);

%% Alpha-Gamma
base_keypoint_size = 3.25; % Value determined for SIFT

% Create descriptor extractor
ag    = vicos.descriptor.AlphaGamma('identifier', 'AG',     'orientation_normalized', true, 'scale_normalized', true, 'compute_orientation', false, 'bilinear_sampling', true, 'use_bitstrings', true, 'non_binarized_descriptor', true,  'num_rays', 13, 'num_circles',  9,  'circle_step', sqrt(2)*1.104, 'base_keypoint_size', base_keypoint_size);

% Compute descriptors
desc1 = ag.compute(I1, kpts1);
desc2 = ag.compute(I2, kpts2);

% Compute distances
D_ag = ag.compute_pairwise_distances(desc1, desc2);


%% Alpha-Gamma, binarized
% Create descriptor extractor
ag60b = vicos.descriptor.AlphaGamma('identifier', 'AG-60B', 'orientation_normalized', true, 'scale_normalized', true, 'compute_orientation', false, 'bilinear_sampling', true, 'use_bitstrings', true, 'non_binarized_descriptor', false, 'num_rays', 23, 'num_circles', 10,  'circle_step',sqrt(2)*1.042, 'base_keypoint_size', base_keypoint_size);

% Compute descriptors
desc1 = ag60b.compute(I1, kpts1);
desc2 = ag60b.compute(I2, kpts2);

% Compute distances
D_ag60b = ag60b.compute_pairwise_distances(desc1, desc2);
