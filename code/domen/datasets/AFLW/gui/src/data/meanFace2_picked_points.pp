<!DOCTYPE PickedPoints>
<PickedPoints>
 <DocumentData>
  <DateTime time="15:31:00" date="2010-08-12"/>
  <User name="koestinger"/>
  <DataFileName name="meanFace2.wrl"/>
  <templateName name="new Template"/>
 </DocumentData>
 <point x="-0.072" y="-0.346" z="-0.333" active="0" name="mouth"/>
 <point x="-0.296" y="-0.348" z="-0.472" active="0" name="mouthLeftCorner"/>
 <point x="0.156" y="-0.366" z="-0.417" active="0" name="mouthRightCorner"/>
 <point x="-0.361" y="0.309" z="-0.521" active="0" name="leftEye"/>
 <point x="-0.4796" y="0.3" z="-0.604" active="0" name="leftEyeLeftCorner"/>
 <point x="-0.221" y="0.296" z="-0.512" active="0" name="leftEyeRightCorner"/>
 <point x="0.235" y="0.296" z="-0.46" active="0" name="rightEye"/>
 <point x="0.105" y="0.291" z="-0.474" active="0" name="rightEyeLeftCorner"/>
 <point x="0.369" y="0.285" z="-0.512" active="0" name="rightEyeRightCorner"/>
 <point x="-0.225169" y="-0.0925778" z="-0.407935" active="0" name="leftNose"/>
 <point x="-0.071" y="-0.023" z="-0.13" active="0" name="nose"/>
 <point x="0.0945821" y="-0.0943744" z="-0.362066" active="0" name="rightNose"/>
 <point x="-0.649" y="-0.198" z="-1.269" active="0" name="leftEar"/>
 <point x="0.593" y="-0.236" z="-1.119" active="0" name="rightEar"/>
 <point x="-0.44" y="0.502" z="-0.469" active="0" name="leftBrowCenter"/>
 <point x="0.283" y="0.486" z="-0.376" active="0" name="rightBrowCenter"/>
 <point x="-0.575" y="0.479" z="-0.64" active="0" name="leftBrowLeftCorner"/>
 <point x="-0.207" y="0.444" z="-0.388" active="0" name="leftBrowRightCorner"/>
 <point x="0.11" y="0.445" z="-0.358" active="0" name="rightBrowLeftCorner"/>
 <point x="0.46" y="0.472" z="-0.51" active="0" name="rightBrowRightCorner"/>
 <point x="-0.068" y="-0.703" z="-0.387" active="0" name="chin"/>
 <point x="-0.0682077" y="-0.284391" z="-0.291888" active="0" name="mouthCenterUpperLipOuterEdge"/>
 <point x="-0.0662728" y="-0.383458" z="-0.318104" active="0" name="mouthCenterLowerLipOuterEdge"/>
 <point x="-0.142852" y="-0.079903" z="-0.269774" active="0" name="leftNoseTril"/>
 <point x="0.0108861" y="-0.0784876" z="-0.259027" active="0" name="rightNoseTril"/>
 <point x="-0.593385" y="0.303609" z="-0.719594" active="0" name="leftTemple"/>
 <point x="0.497715" y="0.276949" z="-0.613964" active="0" name="rightTemple"/>
</PickedPoints>
