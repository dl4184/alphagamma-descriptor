# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/domen/Desktop/alphagamma-descriptor/code/external/dlib-19.1/examples/svm_ex.cpp" "/home/domen/Desktop/alphagamma-descriptor/code/external/dlib-19.1/examples/build/CMakeFiles/svm_ex.dir/svm_ex.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/domen/Desktop/alphagamma-descriptor/code/external/dlib-19.1/dlib/.."
  "/usr/local/include"
  "/usr/local/include/opencv"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/domen/Desktop/alphagamma-descriptor/code/external/dlib-19.1/examples/build/dlib_build/CMakeFiles/dlib.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
