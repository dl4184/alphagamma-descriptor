function [ keypoint_detector, descriptor_extractor, alphagamma_float, alphagamma_short ] = jasna_get_experiment_definition (experiment_id)
    % [ keypoint_detector, native_descriptor, alphagamma_float, alphagamma_short ] = JASNA_GET_EXPERIMENT_DEFINITION (experiment_id)
    %
    % Common settings for keypoint detector/descriptor extractor
    % combinations used in the experiments.
    %
    % Input:
    %  - experiment_id: experiment ID; valid values: surf, sift, brisk, 
    %    orb, kaze, radial
    %
    % Output:
    %  - keypoint_detector: factory function handle for keypoint detector
    %  - descriptor_extractor: factory function handle for native descriptor
    %    extractor (if applicable)
    %  - alphagamma_float: factory function handle for AG, parametrized for
    %    the selected keypoint detector
    %  - alphagamma_short: factory function handle for AGS, parametrized for
    %    the selected keypoint detector
    
    switch lower(experiment_id)
        case 'surf'
            keypoint_detector = @() vicos.keypoint_detector.SURF();
            descriptor_extractor = @() vicos.descriptor.SURF();
            base_keypoint_size = 17.5;
        case 'sift'
            keypoint_detector = @() vicos.keypoint_detector.SIFT();
            descriptor_extractor = @() vicos.descriptor.SIFT();
            base_keypoint_size = 3.25;
        case 'brisk'
            keypoint_detector = @() vicos.keypoint_detector.BRISK();
            descriptor_extractor = @() vicos.descriptor.BRISK();
            base_keypoint_size = 18.5;
        case 'orb'
            keypoint_detector = @() vicos.keypoint_detector.ORB('MaxFeatures', 15000); % OpenCV-default is 500...
            descriptor_extractor = @() vicos.descriptor.ORB();
            base_keypoint_size = 18.5;
        case 'kaze'
            keypoint_detector = @() vicos.keypoint_detector.KAZE();
            descriptor_extractor = @() vicos.descriptor.KAZE('Extended', false);
            base_keypoint_size = 4.75;
        case 'radial'
            keypoint_detector = @() vicos.keypoint_detector.FeatureRadial('MaxFeatures', 15000);
            descriptor_extractor = [];
            base_keypoint_size = [ 8.25, 8.0 ];
        otherwise
            error('Invalid experiment id: "%s"', experiment_id);
    end
    
    % If a single-value base keypoint size is provided, duplicate it
    if isscalar(base_keypoint_size)
        base_keypoint_size(2) = base_keypoint_size;
    end
    
    % Common options for alpha-gamma descriptor: orientation and scale
    % normalization, use external keypoint orientation, apply bilinear
    % sampling, use bitstrings (applicable to binarized version only)
    alphagamma_common_opts = { 'orientation_normalized', true, 'scale_normalized', true, 'compute_orientation', false, 'bilinear_sampling', true, 'use_bitstrings', true };
    alphagamma_float_opts  = [ 'identifier', 'AG',  alphagamma_common_opts, { 'non_binarized_descriptor', true,  'num_rays', 13, 'num_circles',  9,  'circle_step', sqrt(2)*1.104, 'base_keypoint_size', base_keypoint_size(1) } ];
    alphagamma_short_opts  = [ 'identifier', 'AGS', alphagamma_common_opts, { 'non_binarized_descriptor', false, 'num_rays', 23, 'num_circles', 10,  'circle_step', sqrt(2)*1.042, 'base_keypoint_size', base_keypoint_size(2) } ];
    
    alphagamma_float = @() vicos.descriptor.AlphaGamma(alphagamma_float_opts{:});
    alphagamma_short = @() vicos.descriptor.AlphaGamma(alphagamma_short_opts{:});
end