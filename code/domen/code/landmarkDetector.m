 function [ landmarks ] = landmarkDetector( imgLocation, modelLocation, detectorType )
% function that returns detected landmarks for the given image location
% input parameters:
% imgLocation: relative image location inside dataset folder e.g. 'AFLW/data/flickr/0/image00056.jpg'
% modelLocation: relative model location inside model folder e.g. 'C2F-DPM/CDPM.xml'
% detector type:
%   - uricar
%   - kazemi 

DOMEN_FOLDER=getPath();


landmarks= [];
modelLocation = strcat(DOMEN_FOLDER,'/models/',modelLocation);
imgLocation= strcat(DOMEN_FOLDER,'/datasets/',imgLocation);
switch detectorType
    case 'uricar'
        I=imread(imgLocation);
        Ibw = rgb2gray(I);
        
        flandmark = flandmark_class(modelLocation);

        % Set face bbox
        output=face_detection(imgLocation);
        
        bbox = output.face_1;
        landmarks= flandmark.detect(Ibw, int32(bbox));
    case 'kazemi'
        output=face_landmark_detector(modelLocation,imgLocation);
        if output.noDetectedFaces==1
            landmarks= output.face_1;
        end
      
end

