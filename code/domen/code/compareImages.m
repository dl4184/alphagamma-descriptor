function [ D_ag ] = compareImages(I1,points1,imageNo2)
    %Function that returns distance matrix. 
    %I1 is an image
    %points1 are keypoints of I1
    %imageNo2 is index (eg. '0000' is first image, '0001' is the second image from BioID-FaceDatabase)
    
    
    %path to files
    DOMEN_FOLDER=getPath();

    BASE_FILE=strcat(DOMEN_FOLDER,'/datasets/BioID-FaceDB');
    FACE_IMAGES_FILE=strcat(BASE_FILE,'/BioID-FaceDatabase-V1.2');
    KEYPOINTS_FILE=strcat(BASE_FILE,'/bioid_pts/points_20');

    %reading an image

    image_filename2 = strcat(FACE_IMAGES_FILE,'/BioID_',imageNo2,'.pgm');
    I2 = imread(image_filename2);

    %reading points
    %points_file_path1=strcat(KEYPOINTS_FILE,'/bioid_',imageNo1,'.pts');
    %points1=readPTS(points_file_path1);

    points_file_path2=strcat(KEYPOINTS_FILE,'/bioid_',imageNo2,'.pts');
    points2=readPTS(points_file_path2);

    len_points1=size(points1,1);
    len_points2=size(points2,1);

    % Alpha-Gamma
    base_keypoint_size = 5;
    
    %inicialization
    init_struct=struct('pt',[0 0],'size',5,'angle',0);

    kpts1(1:len_points1)=init_struct;
    kpts2(1:len_points2)=init_struct;

    %puting points into a struct
    for i=1:len_points1
        kpts1(i).pt=[points1(i,1) points1(i,2)]; 
    end

    for i=1:len_points2
        kpts2(i).pt=[points2(i,1) points2(i,2)];
    end


    % Create descriptor extractor
    ag    = vicos.descriptor.AlphaGamma('identifier', 'AG','orientation_normalized', false, 'scale_normalized', true, 'compute_orientation', true, 'bilinear_sampling', true, 'use_bitstrings', true, 'non_binarized_descriptor', true,  'num_rays', 13, 'num_circles',  9,  'circle_step', sqrt(2)*1.104, 'base_keypoint_size', base_keypoint_size);


    %Compute descriptors
    desc1 = ag.compute(I1, kpts1);
    desc2 = ag.compute(I2, kpts2);

    %Compute distances
    D_ag = ag.compute_pairwise_distances(desc1, desc2);

end  





