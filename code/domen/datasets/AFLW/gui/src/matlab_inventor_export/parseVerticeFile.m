function [vert] = parseVerticeFile(path)
%function [vert] = parseVerticeFile(path)
%
%Parse a .vertice file from makehuman project
%
%**************************************************************************
vert = [];

[fid, message] = fopen(path, 'r+');
if(fid == -1)
    error(message);
end

while ~feof(fid)
    tline = fgetl(fid);
    tmp = sscanf(tline, '%f %*c %f %*c %f');
    if (length(tmp) ~= 3)
        disp( ['found parse error in vertices at' tline] );
    end
    vert = [vert, tmp];
end

end