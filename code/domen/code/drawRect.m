function [ I ] = drawRect( I,m )
%DRAWRECT draws a react on I
%   m[x y width height]
if size(I,3)<3
    I = cat(3, I, I, I);
end

m=int16(m);

max_y=min(m(2)+m(4),size(I,1));
max_x=min(m(1)+m(3),size(I,2));

I(m(2),m(1):(max_x), 1)=255;
I(m(2),m(1):(max_x), 2)=0;
I(m(2),m(1):(max_x), 3)=0;

I( m(2):(max_y),m(1),1)=255;
I( m(2):(max_y),m(1),2)=0;
I( m(2):(max_y),m(1),3)=0;

I( m(2):(max_y),(max_x),1)=255;
I( m(2):(max_y),(max_x),2)=0;
I( m(2):(max_y),(max_x),3)=0;

I( max_y,m(1):(max_x),1)=255;
I( max_y,m(1):(max_x),2)=0;
I( max_y,m(1):(max_x),3)=0;
end

